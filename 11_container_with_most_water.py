from typing import List
import unittest


class TestIsPalindrome(unittest.TestCase):
    def setUp(self):
        self.sol = Solution()

    def test_1(self):
        testcase = [1, 8, 6, 2, 5, 4, 8, 3, 7]
        expect = 49
        self.assertEqual(self.sol.maxArea(testcase), expect)

    def test_2(self):
        testcase = [1, 1]
        expect = 1
        self.assertEqual(self.sol.maxArea(testcase), expect)

    def test_3(self):
        testcase = [4, 3, 2, 1, 4]
        expect = 16
        self.assertEqual(self.sol.maxArea(testcase), expect)

    def test_4(self):
        testcase = [1, 2, 1]
        expect = 2
        self.assertEqual(self.sol.maxArea(testcase), expect)

    # Failed by comparing adjacent heights
    def test_5(self):
        testcase = [1, 2, 4, 3]
        expect = 4
        self.assertEqual(self.sol.maxArea(testcase), expect)

    # Failed by comparing height increase
    def test_6(self):
        testcase = [1, 3, 2, 5, 25, 24, 5]
        expect = 24
        self.assertEqual(self.sol.maxArea(testcase), expect)


class Solution:
    def maxArea(self, height: List[int]) -> int:
        left = 0
        right = len(height) - 1
        max = 0
        while left < right:
            volume = min([height[left], height[right]]) * (right - left)
            if volume > max:
                max = volume
            # move the smaller side
            if height[left] < height[right]:
                left += 1
            elif height[left] > height[right]:
                right -= 1
            elif height[left+1] > height[right-1]:
                left += 1
            else:
                right -= 1
        return max


if __name__ == '__main__':
    unittest.main()
